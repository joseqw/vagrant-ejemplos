# ej7_vm1

## Aprovisionamiento Básico: 
### Ejecución de comandos en el entorno virtual!

Para entornos virtuales basados en sistemas operativos linux el aprovisionamiento consiste en ejecutar comandos básicos de shell como bash o sh (entre otros).

En este ejemplo, utilizaremos un pequeño script en bash para instalar el servidor nginx al momento que se crea el entorno virtual, al mismo tiempo que cofiguramos la redireción de puertos:

```sh 
$ apt update && apt install -y nginx
```
Configuración en el `Vagrantfile`:
```ruby
config.vm.network "forwarded_port", guest:80, host:8080
config.vm.provision "shell", path: "./install_.sh"
```
script: install_.sh 
```sh
#!/usr/bin/env bash

if [! -x /usr/sbin/nginx]; then
  apt update && apt install -y nginx;
fi 
```

 
