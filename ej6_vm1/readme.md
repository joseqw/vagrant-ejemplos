# ej6_vm1

## Aprovisionamiento Básico: 
### Ejecución de comandos en el entorno virtual!

Para entornos virtuales basados en sistemas operativos linux el aprovisionamiento consiste en ejecutar comandos básicos de shell como bash o sh (entre otros).

En este ejemplo, utilizaremos comando de shell para instalar el servidor nginx al momento que se crea el entorno virtual, al mismo tiempo que cofiguramos la redireción de puertos:

```sh 
# apt update && apt install -y nginx
```
Configuracion en el `Vagrantfile`:
```ruby
config.vm.network "forwarded_port", guest:80, host:8080
config.vm.provision "shell",
    inline: "apt update && apt install -y nginx"
```


 
